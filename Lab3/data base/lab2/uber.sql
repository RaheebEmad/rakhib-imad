postgres=# CREATE TABLE car (
postgres(#  car_id INT PRIMARY KEY NOT NULL ,
postgres(#  car_model VARCHAR(250) ,
postgres(# car_color VARCHAR(250)
postgres(# );
CREATE TABLE
postgres=# CREATE TABLE client (
postgres(# id INT PRIMARY KEY NOT NULL,
postgres(#  name VARCHAR(255) NOT NULL,
postgres(#  number VARCHAR(255)
postgres(# );
CREATE TABLE
postgres=# REATE TABLE drivers (
postgres(# driver_id INT PRIMARY KEY NOT NULL ,
postgres(# name VARCHAR(250) ,
postgres(# car_id INT references car(car_id) ,
postgres(#  phone_numberVARCHAR(250)
postgres(# );
ERROR:  syntax error at or near "REATE"
LINE 1: REATE TABLE drivers (
        ^
postgres=# CREATE TABLE drivers (
postgres(# driver_id INT PRIMARY KEY NOT NULL ,
postgres(# name VARCHAR(250) ,
postgres(# car_id INT references car(car_id) ,
postgres(#  phone_numberVARCHAR(250)
postgres(# );
ERROR:  syntax error at or near "("
LINE 5:  phone_numberVARCHAR(250)
                            ^
postgres=# CREATE TABLE drivers (
postgres(# driver_id INT PRIMARY KEY NOT NULL,
postgres(# name VARCHAR(250),
postgres(# car_id INT references car(car_id),
postgres(# phone_number VARCHAR(250)
postgres(# );
CREATE TABLE
postgres=# \dt
          List of relations
 Schema |  Name   | Type  |  Owner
--------+---------+-------+----------
 public | car     | table | postgres
 public | client  | table | postgres
 public | drivers | table | postgres
(3 rows)


postgres=# \d
          List of relations
 Schema |  Name   | Type  |  Owner
--------+---------+-------+----------
 public | car     | table | postgres
 public | client  | table | postgres
 public | drivers | table | postgres
(3 rows)


postgres=# CREATE TABLE trip (
postgres(# id INT primary KEY NOT NULL,
postgres(# driver_id  INT references drivers (driver_id),
postgres(# client_id INT references client (id),
postgres(# dateAndTime TimeStamp
postgres(# );
CREATE TABLE
postgres=# \dt
          List of relations
 Schema |  Name   | Type  |  Owner
--------+---------+-------+----------
 public | car     | table | postgres
 public | client  | table | postgres
 public | drivers | table | postgres
 public | trip    | table | postgres
(4 rows)


postgres=# INSERT INTO car VALUES (1 , 'MUSTENG' ,'red');
INSERT 0 1
postgres=# INSERT INTO car VALUES (2 , 'ford' ,'bule');
INSERT 0 1
postgres=# INSERT INTO car VALUES (3 , 'bmw' ,'black');
INSERT 0 1
postgres=# INSERT INTO car VALUES (4 , 'KIA' ,'GREEN');
INSERT 0 1
postgres=# INSERT INTO car VALUES (5 , 'BENS' ,'BROWN');
INSERT 0 1
postgres=# INSERT INTO car VALUES (6 , 'KIA' ,'RED');
INSERT 0 1
postgres=# INSERT INTO car VALUES (7 , 'HYUNDAI' ,'ORANGE');
INSERT 0 1
postgres=# INSERT INTO car VALUES (8 , 'HYUNDAI' ,'RED');
INSERT 0 1
postgres=# INSERT INTO car VALUES (9 , 'KIA' ,'BLUE');
INSERT 0 1
postgres=# INSERT INTO car VALUES (10 , 'FERAIRE' ,'BLUE');
INSERT 0 1
postgres=# INSERT INTO client VALUES (1 , 'moha','65120564');
INSERT 0 1
postgres=# INSERT INTO client VALUES (2 , 'ali','651257');
INSERT 0 1
postgres=# INSERT INTO client VALUES (3 , 'mohamed','54125457');
INSERT 0 1
postgres=# INSERT INTO client VALUES (4 , 'mslam','064525457');
INSERT 0 1
postgres=# INSERT INTO client VALUES (5 , 'sara','6545457');
INSERT 0 1
postgres=# INSERT INTO client VALUES (6 , 'lamees','065145457');
INSERT 0 1
postgres=# INSERT INTO client VALUES (7 , 'sacha','875145457');
INSERT 0 1
postgres=# INSERT INTO client VALUES (8 , 'mechoo','2135457');
INSERT 0 1
postgres=# INSERT INTO client VALUES (9 , 'mode','6512217');
INSERT 0 1
postgres=# INSERT INTO client VALUES (10 , 'mosa','06512217');
INSERT 0 1
postgres=#
postgres=#
postgres=# INSERT INTO drivers VALUES (1 ,'ali' ,10 , '6512354' );
INSERT 0 1
postgres=# INSERT INTO drivers VALUES (2 ,'moha' ,9, '06512354' );
INSERT 0 1
postgres=# INSERT INTO drivers VALUES (3 ,'salm' ,8, '06512354' );
INSERT 0 1
postgres=# INSERT INTO drivers VALUES (4 ,'soha' ,7, '06512354' );
INSERT 0 1
postgres=# INSERT INTO drivers VALUES (5 ,'sara' ,6, '506512354' );
INSERT 0 1
postgres=# INSERT INTO drivers VALUES (6 ,'hezzam' ,5, '5065123' );
INSERT 0 1
postgres=# INSERT INTO drivers VALUES (7 ,'ali' ,4, '651616' );
INSERT 0 1
postgres=# INSERT INTO drivers VALUES (8 ,'sal' ,3, '60241551616' );
INSERT 0 1
postgres=# INSERT INTO drivers VALUES (9 ,'medoo' ,2, '601551616' );
INSERT 0 1
postgres=# INSERT INTO drivers VALUES (10 ,'shosh' ,1, '601551616' );
INSERT 0 1
postgres=#
postgres=#
postgres=# INSERT INTO trip VALUES (1,2,3,'2021-7-1 22:54:00');
INSERT 0 1
postgres=# INSERT INTO trip VALUES (2,3,4,'2021-7-2 20:52:00');
INSERT 0 1
postgres=# INSERT INTO trip VALUES (3,4,5,'2021-7-3 12:21:00');
INSERT 0 1
postgres=# INSERT INTO trip VALUES (4,5,6,'2021-7-3 11:01:00');
INSERT 0 1
postgres=# INSERT INTO trip VALUES (5,6,7,'2021-7-3 12:01:00');
INSERT 0 1
postgres=# INSERT INTO trip VALUES (6,7,8,'2021-7-3 12:21:00');
INSERT 0 1
postgres=# INSERT INTO trip VALUES (7,8,9,'2021-7-3 12:21:00');
INSERT 0 1
postgres=# INSERT INTO trip VALUES (8,9,10,'2021-7-3 20:21:00');
INSERT 0 1
postgres=# INSERT INTO trip VALUES (9,10,1,'2021-7-3 18:21:00');
INSERT 0 1
postgres=# INSERT INTO trip VALUES (10,1,2,'2021-7-3 18:21:00');
INSERT 0 1
postgres=# \dt
          List of relations
 Schema |  Name   | Type  |  Owner
--------+---------+-------+----------
 public | car     | table | postgres
 public | client  | table | postgres
 public | drivers | table | postgres
 public | trip    | table | postgres
(4 rows)


postgres=# SELECT  FROM car;
--
(10 rows)


postgres=# SELECT * FROM car;
 car_id | car_model | car_color
--------+-----------+-----------
      1 | MUSTENG   | red
      2 | ford      | bule
      3 | bmw       | black
      4 | KIA       | GREEN
      5 | BENS      | BROWN
      6 | KIA       | RED
      7 | HYUNDAI   | ORANGE
      8 | HYUNDAI   | RED
      9 | KIA       | BLUE
     10 | FERAIRE   | BLUE
(10 rows)


postgres=# SELECT * FROM drivers;
 driver_id |  name  | car_id | phone_number
-----------+--------+--------+--------------
         1 | ali    |     10 | 6512354
         2 | moha   |      9 | 06512354
         3 | salm   |      8 | 06512354
         4 | soha   |      7 | 06512354
         5 | sara   |      6 | 506512354
         6 | hezzam |      5 | 5065123
         7 | ali    |      4 | 651616
         8 | sal    |      3 | 60241551616
         9 | medoo  |      2 | 601551616
        10 | shosh  |      1 | 601551616
(10 rows)


postgres=# SELECT * FROM client;
 id |  name   |  number
----+---------+-----------
  1 | moha    | 65120564
  2 | ali     | 651257
  3 | mohamed | 54125457
  4 | mslam   | 064525457
  5 | sara    | 6545457
  6 | lamees  | 065145457
  7 | sacha   | 875145457
  8 | mechoo  | 2135457
  9 | mode    | 6512217
 10 | mosa    | 06512217
(10 rows)


postgres=# SELECT * FROM trip;
 id | driver_id | client_id |     dateandtime
----+-----------+-----------+---------------------
  1 |         2 |         3 | 2021-07-01 22:54:00
  2 |         3 |         4 | 2021-07-02 20:52:00
  3 |         4 |         5 | 2021-07-03 12:21:00
  4 |         5 |         6 | 2021-07-03 11:01:00
  5 |         6 |         7 | 2021-07-03 12:01:00
  6 |         7 |         8 | 2021-07-03 12:21:00
  7 |         8 |         9 | 2021-07-03 12:21:00
  8 |         9 |        10 | 2021-07-03 20:21:00
  9 |        10 |         1 | 2021-07-03 18:21:00
 10 |         1 |         2 | 2021-07-03 18:21:00
(10 rows)


postgres=#