postgres=# DROP DATABASE uber;
DROP DATABASE
postgres=# CREATE DATABASE car;
CREATE DATABASE
postgres=# \c car;
You are now connected to database "car" as user "postgres".
car=# create table cars (
car(# id BIGSERIAL NOT NULL PRIMARY KEY,
car(# car_model VARCHAR(50) NOT NULL,
car(# car_color VARCHAR(50) NOT NULL
car(# );
CREATE TABLE
car=# insert into cars (id, car_model, car_color) values (1, 'CL-Class', 'Orange');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (2, 'Stratus', 'Fuscia');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (4, 'Kizashi', 'Khaki');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (5, 'Excursion', 'Orange');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (6, 'Town & Country', 'Yellow');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (7, 'Ram Van 2500', 'Mauv');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (8, 'Sonoma', 'Green');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (9, 'Maxima', 'Aquamarine');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (10, 'Suburban 2500', 'Khaki');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (11, 'Firefly', 'Crimson');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (12, 'F-Series', 'Yellow');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (13, 'Eclipse', 'Mauv');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (14, 'Maxima', 'Maroon');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (15, 'F250', 'Red');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (16, 'Camaro', 'Red');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (17, 'Boxster', 'Turquoise');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (18, 'C70', 'Fuscia');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (19, 'Freestyle', 'Mauv');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (20, 'Cavalier', 'Purple');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (21, 'Jimmy', 'Fuscia');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (22, 'Grand Am', 'Red');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (23, 'MX-3', 'Purple');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (24, 'New Yorker', 'Red');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (25, 'S40', 'Goldenrod');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (26, 'J', 'Crimson');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (27, 'Navigator', 'Maroon');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (28, 'Lancer', 'Red');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (29, 'Genesis', 'Turquoise');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (30, 'Savana 1500', 'Violet');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (31, 'Sonata', 'Pink');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (32, 'F250', 'Khaki');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (33, 'Express 2500', 'Teal');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (34, 'Lancer', 'Orange');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (35, '940', 'Purple');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (36, 'Navigator', 'Puce');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (37, 'Solara', 'Yellow');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (37, 'Solara', 'Yellow');
ERROR:  duplicate key value violates unique constraint "cars_pkey"
DETAIL:  Key (id)=(37) already exists.
car=# insert into cars (id, car_model, car_color) values (39, 'Camaro', 'Aquamarine');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (40, 'tC', 'Fuscia');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (41, 'Sentra', 'Orange');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (42, 'SC', 'Orange');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (43, 'Civic', 'Pink');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (44, 'Ram 2500', 'Aquamarine');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (45, 'Vibe', 'Puce');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (46, 'Fusion', 'Orange');
INSERT 0 1
car=# insert into cars (id, car_model, car_color) values (47, 'Express 2500', 'Fuscia');
INSERT 0 1
car=#
car=#
car=#
car=# SELECT * FROM car;
ERROR:  relation "car" does not exist
LINE 1: SELECT * FROM car;
                      ^
car=# SELECT * FROM cars;
 id |   car_model    | car_color
----+----------------+------------
  1 | CL-Class       | Orange
  2 | Stratus        | Fuscia
  4 | Kizashi        | Khaki
  5 | Excursion      | Orange
  6 | Town & Country | Yellow
  7 | Ram Van 2500   | Mauv
  8 | Sonoma         | Green
  9 | Maxima         | Aquamarine
 10 | Suburban 2500  | Khaki
 11 | Firefly        | Crimson
 12 | F-Series       | Yellow
 13 | Eclipse        | Mauv
 14 | Maxima         | Maroon
 15 | F250           | Red
 16 | Camaro         | Red
 17 | Boxster        | Turquoise
 18 | C70            | Fuscia
 19 | Freestyle      | Mauv
 20 | Cavalier       | Purple
 21 | Jimmy          | Fuscia
 22 | Grand Am       | Red
 23 | MX-3           | Purple
 24 | New Yorker     | Red
 25 | S40            | Goldenrod
 26 | J              | Crimson
 27 | Navigator      | Maroon
 28 | Lancer         | Red
 29 | Genesis        | Turquoise
 30 | Savana 1500    | Violet
 31 | Sonata         | Pink
 32 | F250           | Khaki
 33 | Express 2500   | Teal
 34 | Lancer         | Orange
 35 | 940            | Purple
 36 | Navigator      | Puce
 37 | Solara         | Yellow
 39 | Camaro         | Aquamarine
 40 | tC             | Fuscia
 41 | Sentra         | Orange
 42 | SC             | Orange
 43 | Civic          | Pink
 44 | Ram 2500       | Aquamarine
 45 | Vibe           | Puce
 46 | Fusion         | Orange
 47 | Express 2500   | Fuscia
(45 rows)


car=#
car=#
car=#