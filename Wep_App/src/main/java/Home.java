import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

@WebServlet("/index")
public class Home extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {

        Cookie[] cookies = httpServletRequest.getCookies();
        if (cookies != null) {
            for (Cookie ck : cookies) {
                if (ck.getName().equals("SessionID")) {
                    try {
                        sendToHomePage(httpServletRequest, httpServletResponse, ck);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    return;
                }
            }
        }

        httpServletRequest.getRequestDispatcher("/Index.html").forward(httpServletRequest, httpServletResponse);

    }

    public static void sendToHomePage(HttpServletRequest request, HttpServletResponse response, Cookie ck) throws SQLException, ServletException, IOException {
        Connection conn = DataBase.getConnection();

        try {
            Statement st = conn.createStatement();
            ResultSet result = st.executeQuery("select * from users_test where cooki_id = '" + ck.getValue() + "';");
            //String[] attributes = new String[]{"First_Name", "Last_Name"};
            while (result.next()) {
                request.setAttribute("First_Name", result.getString(1));
                request.setAttribute("Last_Name", result.getString(2));
            }
            request.getRequestDispatcher("/Home.ftlh").forward(request, response);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String setCookie(HttpServletResponse response) {
        String SessionID = UUID.randomUUID().toString();
        Cookie ck = new Cookie("SessionID", SessionID);
        ck.setMaxAge(600);
        response.addCookie(ck);
        return SessionID;
    }

    public static void sendNewCookieID(String email, String sessionID, Statement st) throws SQLException {
        st.executeUpdate("UPDATE users_test SET cooki_id = '"+sessionID+"' WHERE email_address = '"+email+"';");
    }


}
