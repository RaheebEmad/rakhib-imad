import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@WebServlet("/login")
public class Login extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Connection conn = DataBase.getConnection();

        // get parameters
        String email = request.getParameter("Email");
        String pass = request.getParameter("Pass");

        try {
            Statement st = conn.createStatement();

                // check if user exit in database ..
                if (checkUser(email, pass, st)) {
                    ResultSet result = st.executeQuery("select * from users_test where email_address = '" + email + "';");
                    while (result.next()) {
                        request.setAttribute("First_Name", result.getString(1));
                        request.setAttribute("Last_Name", result.getString(2));
                    }
                    String SessionID = Home.setCookie(response);
                    // for chang cookie_id into database
                    Home.sendNewCookieID(email, SessionID, st);
                    request.getRequestDispatcher("/Home.ftlh").forward(request, response);
                } else {
                    request.getRequestDispatcher("/NotFoundUser.html").forward(request, response);
                }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    private boolean checkUser(String email, String pass, Statement st) throws SQLException {
        ResultSet e = st.executeQuery("select exists(select email_address,password from users_test where email_address = '" + email + "' and password = '" + pass + "');");
        e.next();
        return e.getString(1).equals("t");
    }
}
